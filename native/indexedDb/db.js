import Dexie from 'dexie';
let db = new Dexie("intangles");
  
//Database schemas
db.version(1).stores({
    vehicles : "&id,&v_id,account_id",
    specs: "&id",
    log: "vehicleList",
    accountToSpecs: "&id"
})
db.version(2).stores({
    vehicles : "&id,&v_id,account_id",
    specs: "&id",
    log: "vehicleList",
    accounts: "&id,current",
    accountToSpecs: "&id",
    appconfig: "&current_account_id"
});
db.version(3).stores({
    vehicles: "&id,&v_id,account_id,tagged,obd",
    specs: "&id",
    log: "vehicleList",
    accounts: "&id,current",
    accountToSpecs: "&id",
    appconfig: "&current_account_id"
});
//

export default db;