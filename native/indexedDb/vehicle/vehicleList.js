import fetchVehicleFilter from "../../apis/fetchVehicleFilter";
import db from 'shared/db';
import { Promise } from "core-js";
export default async function (accountId, pnum, psize, query, lastLoc, obd, noMapView, sorting, options) {
    if (!db)
        return await fetchVehicleFilter(pnum, psize, query, lastLoc, obd, noMapView, sorting, options, accountId);
    var offset = (pnum - 1) * psize;
    var q = null;
    if (query && query.query && query.query.length > 0) {
        q = query.query;
    }
    var obj = { account_id: accountId };
    if (obd)
        obj.obd = true;

    function filter(vehicle) {
        if (q && q != '*') {
            return vehicle.tagged.indexOf(q) > -1;
        }
        else if (q == "*")
            return true;
        return true;
    }
    try {
        await db.open();
        let [ list, count] = await Promise.all([ 
            db.vehicles.where(obj).filter(filter).offset(offset).limit(psize).toArray(), 
            db.vehicles.where({ account_id: accountId }).count() 
        ]) ;
        await db.close();
        return {
            v: list,
            paging: {
                total: count,
                start: offset + 1,
                end: offset + psize,
                pnum: pnum,
                psize: psize
            }
        }
    }
    catch(e) {
        console.log(e);
        return await fetchVehicleFilter(pnum, psize, query, lastLoc, obd, noMapView, sorting, options, accountId);
    }
}