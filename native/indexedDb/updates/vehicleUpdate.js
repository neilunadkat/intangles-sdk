
module.exports = {
    startUpdating : startUpdating,
    triggerUpdate : triggerUpdate
}
function startUpdating(db,triggerEvent){
    if(db)
    window.x = setTimeout(function(){
        triggerUpdate(db,triggerEvent);
    }, 5* 60 *1000)
    else
    return;
}

function triggerUpdate(db,triggerEvent){
    //triggerEvent("vehicleChanged");
    if(!db)
    return;
    db.open().catch(function(error) {
        console.error('Uh oh : ' + error);
    });
    console.log('updating function start');
    db.accounts.where("current").equals(1).toArray(function(data){
        if(data[0])
        {
            updateDataForAccount(db,data[0]).then(function(){
                startUpdating(db,triggerEvent);
                return;
            }).catch(function(error){
                console.log("Some error occured. Retrying update.",error);
                startUpdating(db,triggerEvent);
                return;
            })
        };
    });
}

function updateDataForAccount(db,account)
{
    return new Promise(function(resolve,reject){
        db.accounts.where("id").equals(account.id).toArray().then(function(acc)
        {
            if(acc[0])
            db.appconfig.where("current_account_id").equals(account.id).toArray().then(function(data){
                if(data[0])
                {
                    fetchAllVehicles(db,acc[0].last_updated,data[0].token,account.id).then(function(data){
                        db.accounts.where("id").equals(account.id).modify({
                            last_updated:new Date()
                        });
                        if(data.length>0)
                        db.vehicles.bulkPut(data).then(function(item){
                            console.log("Done updating for",account.id,new Date());
                            resolve();
                        });
                        else
                        {    
                            console.log("Done updating for",account.id,new Date());
                            resolve();
                        }
                    })
                }
            }).catch(function(error){
                reject(error);
            })
        });
    });
}

function fetchAllVehicles(db,last_updated,token,id,pnum)
{
    return new Promise(function(resolve,reject){
        var data = [];
        var total = 0;
        var pnum = 1;
        do{
            try{
                fetch("https://apis.intangles.com/vehicle/getlist?updatedafter="+last_updated.getTime()+"&token="+token+"&psize=" + 250 + "&pnum=" + pnum + "&acc_id=" + id,{
                    method: 'get'
                }).then(function(body){
                    body.json().then(function(res){
                        var total = res.paging.total;
                        data = data.concat(res.v);
                        pnum++;
                        if(total<=data.length)
                        {
                            data.forEach(function(item){
                                item.account_id = id;
                            });
                            resolve(data);
                        }
                    }).catch(function(error){
                        reject(error);
                    });
                })
            }
            catch(e)
            {
                reject(e);
            }
        }
        while(total>data.length);
    });
}

function getVihicleList(pnum, psize, query, lastLoc, obd, noMapView, sorting,options) {
    var _this = this;

    if (!pnum) pnum = 1;
    if (!psize) psize = 25;
    if (!query) query = "*";

    if (appconfig.user_type === "singleSignIn" && appconfig.vehicles_filter) {
        query = appconfig.vehicles_filter;

    }
    // console.log(query);


    return $q(function (resolve, reject) {


        var url = appconfig.vehicleSvcUrl + "vehicle/getlist?psize=" + psize + "&pnum=" + pnum + "&status=*";
        
        // if(typeof query === 'string'){
        //     url += "&query=" + query;
        // }
        if(typeof query === 'object'){
            if(query.vid)
                url+= "&vid=" +query.vid;
            if(query.query)
                url+= "&query=" + query.query;
        }
        if (lastLoc)
            url = url + "&lastloc=true&drivers=true";
        else
            url = url + "&lastloc=false";
        if (obd) {
            url = url + "&obd=true";
            url = url + "&sort=health desc";
        }
        if (sorting) {
            var sort = '';
            for (var i = 0; i < sorting.length; i++) {
                var sortType = sorting[i].isasc ? 'asc' : 'desc';
                sort += sorting[i].property + ' ' + sortType;
            }
            if (sort && sort !== '') {
                url += "&sort=" + sort;
            }
        }if(options){
            if(options.fuel){
                url += "&fuel=" + options.fuel;
            }
            if(options.msort){
                var msort = '';
                for (var i = 0; i < options.msort.length; i++) {
                    var sortType = options.msort[i].isasc ? 'asc' : 'desc';
                    msort += options.msort[i].property + ' ' + sortType;
                }
                if (msort && msort !== '') {
                    url += "&msort=" + msort;
                }
            }
            if(options.specs)
                url += "&specs=" + options.specs;                          
            if (options.adblue)
                url += "&adblue=" + options.adblue;  
        }                    
        // console.log("Get vehhicle list...");
        get(url).then(function (res) {
            // console.log(res);
            var code;
            if(res.data.status && res.data.status.code)
                code = parseInt(res.data.status.code);
            if(res.data.status && ( code >= 12000 && code < 13000)){
                accountSvc.logout();
                window.location = window.location.origin;
            }
            else{
                var obj = modelTranslator.toUIVehicleList(res.data, noMapView);
                _this.vehicles.list = obj.list;
                _this.vehicles.paging = obj.paging;
                resolve(_this.vehicles);
            }
        });

    });
}