export default function(results) {
    if (results && results.status) {
        var codeDiff = results.status.code - 200;
        if(isNaN(codeDiff) || codeDiff < 0 || codeDiff >= 100)
            return false;
        return true;
    }
    return false;
}