if(!fetch) require('isomorphic-fetch');
export default async (url,{token,accountId} = {}) => {
    if(token)
        url += "&token="+token;
    if(accountId)
        url += "&acc_id="+accountId;
    var response = await fetch(url);
    var results = await response.json();
    return results;
}