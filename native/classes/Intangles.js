import localstorage from "../helpers/localstorage";
export default
    class IntanglesObject {
    constructor(baseUrl, token, storage) {
        this.baseUrl = baseUrl;
        this.token = token;
        if (storage)
            this.storage = storage;
        else
            this.storage = localstorage;
    }
}