import IntanglesObject from '../Intangles';
import localstorage from '../../helpers/localstorage';
export default
class Vehicle extends IntanglesObject{
    setConfig(config = {}) {
        if(config.baseUrl)
            this.baseUrl = config.baseUrl;
        if(config.token)
            this.token = config.token;
        if(config.storage)
            this.storage = config.storage;
        else if(window && window.localStorage)
            this.storage = localstorage;
    }
    constructor({baseUrl, token, storage}={}){
        super({baseUrl, token, storage}={});
        this.getVehicleList = require('./apis/getVehicleList.api.js').default.bind(this) || require('./apis/getVehicleList.api.js').bind(this);
        this.getVehicleInfo = require('./apis/getVehicleInfo.api.js').default.bind(this) || require('./apis/getVehicleInfo.api.js').bind(this);
        this.getHealthAlgoResults = require('./apis/getHealthAlgoResults.api.js').default.bind(this) || require('./apis/getHealthAlgoResults.api.js').bind(this);

    }
}