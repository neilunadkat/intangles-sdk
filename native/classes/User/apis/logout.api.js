export default function(){
    this.storage.removeItem('User');
    this.storage.removeItem('UserToken');
    this.storage.removeItem('UserTokenDate');
    this.setConfig({
        token: null,
        accountId: null
    })
}