import post from '../../../helpers/post';
export default async function (options = {}) {
    var {
        username,
        pwd,
        baseUrl
    } = options;
    try {
        var config = {
            baseUrl: baseUrl || ( (this && this.baseUrl) ? this.baseUrl : 'https://apis.intangles.com/')
        }
        if (config.baseUrl.substr(-1) != '/')
            config.baseUrl += "/";
        var body = {
            username,
            pwd
        }
        var url = config.baseUrl+"v2/user/login";
        var results = await post(url, { body });
        if (!results || !results.status || results.status.code != 200)
            throw results;
        if (this && this.storage) {
            await Promise.all([
                this.storage.setItem("User", JSON.stringify(results.user)),
                this.storage.setItem("UserToken", results.token),
                this.storage.setItem("UserTokenDate", JSON.stringify(new Date().getTime())),
            ])
        }
        var configObj = {
            token: results.token
        };
        if(results.user && !results.user.is_intangles_user && results.accountId)
            configObj.accountId = results.accountId;
        if(this && this.setConfig)
            this.setConfig(configObj);
        return results;
    }
    catch(e){
        throw e;
    }
};