"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _dexie = require("dexie");

var _dexie2 = _interopRequireDefault(_dexie);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var db = new _dexie2.default("intangles");

//Database schemas
db.version(1).stores({
    vehicles: "&id,&v_id,account_id",
    specs: "&id",
    log: "vehicleList",
    accountToSpecs: "&id"
});
db.version(2).stores({
    vehicles: "&id,&v_id,account_id",
    specs: "&id",
    log: "vehicleList",
    accounts: "&id,current",
    accountToSpecs: "&id",
    appconfig: "&current_account_id"
});
db.version(3).stores({
    vehicles: "&id,&v_id,account_id,tagged,obd",
    specs: "&id",
    log: "vehicleList",
    accounts: "&id,current",
    accountToSpecs: "&id",
    appconfig: "&current_account_id"
});
//

exports.default = db;
//# sourceMappingURL=db.js.map