"use strict";

var fetchAllVehicles = require("store/common/fetchAllVehicles");
var fetchSpecs = require("store/common/fetchSpecs");
// Vehicle list exists in db.vehicles

module.exports = {
    startForceUpdating: startForceUpdating,
    triggerForceUpdate: triggerForceUpdate
};

function startForceUpdating(db, appconf) {
    if (db) window.x = setTimeout(function () {
        triggerForceUpdate(db, appconf).then(function () {
            startForceUpdating(db, appconf);
            return;
        });
    }, 5 * 60 * 1000);
}

function triggerForceUpdate(db, appconfig) {
    if (db) {
        return new Promise(function (resolve, reject) {
            var id = appconfig.current_account_id;
            if (id) {
                fetchAllVehicles(appconfig, id).then(function (list) {
                    db.vehicles.bulkPut(list).then(function (lastKey) {
                        console.log("Vehicles updated");
                        db.accounts.put({
                            id: id,
                            current: 1,
                            last_updated: new Date()
                        }).then(function (done) {
                            console.log("Added or Updated");
                        }).catch(function (error) {
                            console.log('error', error);
                        });
                    });
                });
                fetchSpecs(appconfig, id, db).then(function (specs) {
                    if (specs) {
                        db.specs.bulkPut(specs).then(function (lastKey) {
                            console.log("Specs updated");
                            resolve();
                        });
                    } else {
                        resolve();
                    }
                });
            } else resolve();
        });
    }
}
//# sourceMappingURL=forceUpdate.js.map