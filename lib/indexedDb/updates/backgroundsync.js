"use strict";

var request = require("store/common/getPost");
var forceupdate = require("./forceUpdate");

function sync(db, appconfig) {
    if (!db) return;
    db.accounts.where("current").equals(1).toArray(function (data) {
        if (data[0]) {
            var current_timestamp = new Date().getTime();
            //2400000
            var checktime = current_timestamp - 1800000;
            console.log('checktime', new Date(checktime));
            if (checktime > data[0].last_updated.getTime()) {
                forceupdate.triggerForceUpdate(db, appconfig);
            }
        };
    });
}

module.exports = sync;
//# sourceMappingURL=backgroundsync.js.map