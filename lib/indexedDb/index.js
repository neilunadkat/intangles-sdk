"use strict";

var Dexie = require("dexie").default;
var Vehicle = require("./classes/Vehicle");
var vehicleUpdate = require("./updates/vehicleUpdate");
var forceUpdate = require("./updates/forceUpdate");
var events = {};
var db;
//
var appconfig = {};
var vehicleList = require('./vehicle/vehicleList');
var setAccountData = require('./account/setAccountData');
var getSpecs = require('./account/specs');
var getMileageInfo = "";
var backgroundsync = require("./updates/backgroundsync");
var obj = {
    vehicleList: vehicleList,
    getSpecs: getSpecs,
    getMileageInfo: getMileageInfo,
    setAccountData: setAccountData,
    vehicleUpdate: vehicleUpdate,
    forceUpdate: forceUpdate
};

var on = function on(event, listener) {
    events[event] = listener;
};
var deleteDB = function deleteDB() {
    if (db) db.close();
};
var triggerEvent = function triggerEvent(event, payload) {
    if (events[event] && payload) events[event](payload);else if (events[event]) events[event]();else {
        console.error("Event not found: " + event);
    }
    return;
};
var openDB = function openDB() {
    db = new Dexie("intangles");

    //Database schemas
    db.version(1).stores({
        vehicles: "&id,&v_id,account_id",
        specs: "&id",
        log: "vehicleList",
        accountToSpecs: "&id"
    });
    db.version(2).stores({
        vehicles: "&id,&v_id,account_id",
        specs: "&id",
        log: "vehicleList",
        accounts: "&id,current",
        accountToSpecs: "&id",
        appconfig: "&current_account_id"
    });
    db.version(3).stores({
        vehicles: "&id,&v_id,account_id,tagged,obd",
        specs: "&id",
        log: "vehicleList",
        accounts: "&id,current",
        accountToSpecs: "&id",
        appconfig: "&current_account_id"
    });
    //
    db.vehicles.mapToClass(Vehicle);
    db.open().catch(function (error) {
        console.log('Error occured in open db ' + error);
        db = undefined;
    });
    // db = undefined;
    vehicleUpdate.startUpdating(db, triggerEvent);
};
openDB();

module.exports = {
    vehicleList: obj.vehicleList.bind(undefined, db),
    getSpecs: obj.getSpecs.bind(undefined, db),
    setAccountData: obj.setAccountData.bind(undefined, db),
    // forceUpdate : obj.forceUpdate.bind(this,db),
    //getMileageInfo : obj.getMileageInfo.bind(this,db),
    setConfig: function setConfig(appconf) {
        this.vehicleList = obj.vehicleList.bind(this, db, appconf);
        this.getSpecs = obj.getSpecs.bind(this, db);
        this.setAccountData = obj.setAccountData.bind(this, db, appconf);
        this.vehicleUpdate = obj.vehicleUpdate.triggerUpdate.bind(this, db, triggerEvent);
        this.startforceUpdate = obj.forceUpdate.startForceUpdating.bind(this, db, appconf);
        this.forceUpdate = obj.forceUpdate.triggerForceUpdate.bind(this, db, appconf);
        backgroundsync(db, appconf);
    },
    deleteDB: deleteDB,
    openDB: openDB,
    on: on,
    vehicleUpdate: obj.vehicleUpdate.triggerUpdate.bind(undefined, db, triggerEvent),
    forceUpdate: obj.forceUpdate.triggerForceUpdate.bind(undefined, db),
    startforceUpdate: obj.forceUpdate.startForceUpdating.bind(undefined, db)
};
//# sourceMappingURL=index.js.map