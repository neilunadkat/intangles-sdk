'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _db = require('shared/db');

var _db2 = _interopRequireDefault(_db);

var _getToken = require('shared/helpers/getToken');

var _getToken2 = _interopRequireDefault(_getToken);

var _specs = require('./specs');

var _specs2 = _interopRequireDefault(_specs);

var _fetchAllVehicles = require('shared/apis/fetchAllVehicles');

var _fetchAllVehicles2 = _interopRequireDefault(_fetchAllVehicles);

var _fetchSpecs = require('shared/apis/fetchSpecs');

var _fetchSpecs2 = _interopRequireDefault(_fetchSpecs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee(accountId) {
        var appconfig, iDbVehicles, fetchedVehicles, lastPutKey, updatedCurrentAccount, accountToSpecs, specs, lastSpecPutKey, res;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;

                        if (!(_db2.default == undefined)) {
                            _context.next = 5;
                            break;
                        }

                        _context.next = 4;
                        return (0, _fetchSpecs2.default)(accountId);

                    case 4:
                        return _context.abrupt('return', _context.sent);

                    case 5:
                        appconfig = {
                            authToken: (0, _getToken2.default)(),
                            current_account_id: accountId
                        };
                        _context.next = 8;
                        return _db2.default.open();

                    case 8:
                        _db2.default.accounts.where("id").notEqual(accountId).modify(function (acc) {
                            acc.current = 0;
                        }, function (error) {
                            console.log(error);
                        });
                        _context.next = 11;
                        return _db2.default.vehicles.where({ account_id: accountId }).toArray();

                    case 11:
                        iDbVehicles = _context.sent;

                        if (!(!iDbVehicles || iDbVehicles.length == 0)) {
                            _context.next = 23;
                            break;
                        }

                        _context.next = 15;
                        return (0, _fetchAllVehicles2.default)(accountId);

                    case 15:
                        fetchedVehicles = _context.sent;
                        _context.next = 18;
                        return _db2.default.vehicles.bulkPut(fetchedVehicles);

                    case 18:
                        lastPutKey = _context.sent;
                        updatedCurrentAccount = _db2.default.accounts.put({
                            id: accountId,
                            current: 1,
                            last_updated: new Date()
                        });

                        console.log("Added or Updated");
                        _context.next = 25;
                        break;

                    case 23:
                        _db2.default.accounts.where("id").equals(accountId).modify({
                            current: 1
                        });
                        console.log("Using vehicles from localstorage");

                    case 25:
                        _context.next = 27;
                        return _db2.default.appconfig.clear();

                    case 27:
                        _db2.default.appconfig.add({
                            current_account_id: accountId,
                            token: (0, _getToken2.default)()
                        }).catch(function (error) {
                            console.log(error);
                        });
                        _context.next = 30;
                        return _db2.default.accountToSpecs.where({ id: accountId }).toArray();

                    case 30:
                        accountToSpecs = _context.sent;

                        if (!(!accountToSpecs || accountToSpecs.length == 0)) {
                            _context.next = 49;
                            break;
                        }

                        _context.next = 34;
                        return (0, _fetchSpecs2.default)(accountId);

                    case 34:
                        specs = _context.sent;

                        if (!specs) {
                            _context.next = 44;
                            break;
                        }

                        _context.next = 38;
                        return _db2.default.specs.bulkPut(specs);

                    case 38:
                        lastSpecPutKey = _context.sent;
                        _context.next = 41;
                        return _db2.default.close();

                    case 41:
                        return _context.abrupt('return', specs);

                    case 44:
                        _context.next = 46;
                        return _db2.default.close();

                    case 46:
                        return _context.abrupt('return');

                    case 47:
                        _context.next = 56;
                        break;

                    case 49:
                        console.log("Using specs from localstorage");
                        _context.next = 52;
                        return (0, _specs2.default)(accountId);

                    case 52:
                        res = _context.sent;
                        _context.next = 55;
                        return _db2.default.close();

                    case 55:
                        return _context.abrupt('return', {
                            specs: res
                        });

                    case 56:
                        _context.next = 63;
                        break;

                    case 58:
                        _context.prev = 58;
                        _context.t0 = _context['catch'](0);
                        _context.next = 62;
                        return (0, _fetchSpecs2.default)(accountId);

                    case 62:
                        return _context.abrupt('return', _context.sent);

                    case 63:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 58]]);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=setAccountData.js.map