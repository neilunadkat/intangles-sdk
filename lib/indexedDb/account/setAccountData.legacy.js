"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _db = require("../db");

var _db2 = _interopRequireDefault(_db);

var _getToken = require("shared/helpers/getToken");

var _getToken2 = _interopRequireDefault(_getToken);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var fetchAllVehicles = require("store/common/fetchAllVehicles");
var fetchSpecs = require("store/common/fetchSpecs");
// Vehicle list exists in db.vehicles
var getSpecsFromLocal = require("./specs");

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee(accountId) {
        var appconfig;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        appconfig = {
                            authToken: (0, _getToken2.default)(),
                            current_account_id: accountId
                        };
                        _context.next = 3;
                        return _db2.default.open();

                    case 3:
                        if (!(_db2.default == undefined)) {
                            _context.next = 5;
                            break;
                        }

                        return _context.abrupt("return", fetchSpecs(appconfig, accountId, _db2.default));

                    case 5:
                        return _context.abrupt("return", new Promise(function (resolve, reject) {
                            _db2.default.accounts.where("id").notEqual(accountId).modify(function (acc) {
                                acc.current = 0;
                            }, function (error) {
                                console.log(error);
                            });
                            _db2.default.vehicles.where({ account_id: accountId }).toArray(function (res) {
                                if (!res || res.length == 0) {
                                    fetchAllVehicles(appconfig, accountId).then(function (list) {
                                        _db2.default.vehicles.bulkPut(list).then(function (lastKey) {
                                            console.log(lastKey);
                                            _db2.default.accounts.put({
                                                id: accountId,
                                                current: 1,
                                                last_updated: new Date()
                                            }).then(function (done) {
                                                console.log("Added or Updated");
                                            }).catch(function (error) {
                                                console.log('error', error);
                                            });
                                        });
                                    });
                                } else {
                                    _db2.default.accounts.where("id").equals(accountId).modify({
                                        current: 1
                                    });
                                    console.log("Using vehicles from localstorage");
                                }
                            }, function (error) {
                                console.log(error);
                            });
                            _db2.default.appconfig.clear().then(function () {
                                checkCurrent();
                            }).catch(function () {
                                checkCurrent();
                            });

                            function checkCurrent() {
                                if (!appconfig.current_account_id) {
                                    _db2.default.appconfig.add({
                                        current_account_id: accountId,
                                        token: appconfig.authToken
                                    }).catch(function (error) {
                                        console.log(error);
                                    });
                                } else {
                                    _db2.default.appconfig.add({
                                        current_account_id: appconfig.current_account_id,
                                        token: appconfig.authToken
                                    }).catch(function (error) {
                                        console.log(error);
                                    });
                                }
                            }
                            _db2.default.accountToSpecs.where({ id: accountId }).toArray(function (res) {
                                if (!res || res.length == 0) {
                                    fetchSpecs(appconfig, accountId, _db2.default).then(function (specs) {
                                        if (specs) {
                                            _db2.default.specs.bulkPut(specs).then(function (lastKey) {
                                                resolve(specs);
                                            });
                                        } else {
                                            resolve();
                                        }
                                    });
                                } else {
                                    console.log("Using specs from localstorage");
                                    getSpecsFromLocal(_db2.default, account).then(function (res) {
                                        resolve({ specs: res });
                                    });
                                }
                            }, function (error) {
                                console.log(error);
                            });
                        }));

                    case 6:
                    case "end":
                        return _context.stop();
                }
            }
        }, _callee, this);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=setAccountData.legacy.js.map