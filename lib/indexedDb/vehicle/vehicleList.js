"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _fetchVehicleFilter = require("../../apis/fetchVehicleFilter");

var _fetchVehicleFilter2 = _interopRequireDefault(_fetchVehicleFilter);

var _db = require("shared/db");

var _db2 = _interopRequireDefault(_db);

var _coreJs = require("core-js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new _coreJs.Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return _coreJs.Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee(accountId, pnum, psize, query, lastLoc, obd, noMapView, sorting, options) {
        var offset, q, obj, filter, _ref2, _ref3, list, count;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        filter = function filter(vehicle) {
                            if (q && q != '*') {
                                return vehicle.tagged.indexOf(q) > -1;
                            } else if (q == "*") return true;
                            return true;
                        };

                        if (_db2.default) {
                            _context.next = 5;
                            break;
                        }

                        _context.next = 4;
                        return (0, _fetchVehicleFilter2.default)(pnum, psize, query, lastLoc, obd, noMapView, sorting, options, accountId);

                    case 4:
                        return _context.abrupt("return", _context.sent);

                    case 5:
                        offset = (pnum - 1) * psize;
                        q = null;

                        if (query && query.query && query.query.length > 0) {
                            q = query.query;
                        }
                        obj = { account_id: accountId };

                        if (obd) obj.obd = true;

                        _context.prev = 10;
                        _context.next = 13;
                        return _db2.default.open();

                    case 13:
                        _context.next = 15;
                        return _coreJs.Promise.all([_db2.default.vehicles.where(obj).filter(filter).offset(offset).limit(psize).toArray(), _db2.default.vehicles.where({ account_id: accountId }).count()]);

                    case 15:
                        _ref2 = _context.sent;
                        _ref3 = _slicedToArray(_ref2, 2);
                        list = _ref3[0];
                        count = _ref3[1];
                        _context.next = 21;
                        return _db2.default.close();

                    case 21:
                        return _context.abrupt("return", {
                            v: list,
                            paging: {
                                total: count,
                                start: offset + 1,
                                end: offset + psize,
                                pnum: pnum,
                                psize: psize
                            }
                        });

                    case 24:
                        _context.prev = 24;
                        _context.t0 = _context["catch"](10);

                        console.log(_context.t0);
                        _context.next = 29;
                        return (0, _fetchVehicleFilter2.default)(pnum, psize, query, lastLoc, obd, noMapView, sorting, options, accountId);

                    case 29:
                        return _context.abrupt("return", _context.sent);

                    case 30:
                    case "end":
                        return _context.stop();
                }
            }
        }, _callee, this, [[10, 24]]);
    }));

    return function (_x, _x2, _x3, _x4, _x5, _x6, _x7, _x8, _x9) {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=vehicleList.js.map