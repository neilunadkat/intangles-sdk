'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require('../../../helpers/get');

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var name, modelName, query, is_aux, pnum, psize, noThrow, config, url, results;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        name = options.name, modelName = options.modelName, query = options.query, is_aux = options.is_aux, pnum = options.pnum, psize = options.psize, noThrow = options.noThrow;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();
                        url = config.baseUrl + 'idevice/model/list?';

                        if (query) url += '&query=' + query;
                        if (name && !modelName) url += '&name=' + name;
                        if (modelName && !name) url += '&name=' + modelName;
                        is_aux ? url += '&is_aux=' + is_aux : null;
                        if (!pnum) pnum = 1;
                        if (!psize) psize = 20;

                        url += '&pnum=' + pnum + '&psize=' + psize;
                        _context.next = 13;
                        return (0, _get2.default)(url, { token: config.token });

                    case 13:
                        results = _context.sent;

                        if (!noThrow) {
                            _context.next = 16;
                            break;
                        }

                        return _context.abrupt('return', results);

                    case 16:
                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 18;
                            break;
                        }

                        throw results;

                    case 18:
                        return _context.abrupt('return', results);

                    case 21:
                        _context.prev = 21;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 24:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 21]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getDeviceModelList.api.js.map