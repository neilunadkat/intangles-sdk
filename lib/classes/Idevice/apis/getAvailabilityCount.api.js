'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require('../../../helpers/get');

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var deployed, _options$statusReason, statusReasons, pnum, psize, noThrow, config, url, results;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        deployed = options.deployed, _options$statusReason = options.statusReasons, statusReasons = _options$statusReason === undefined ? [] : _options$statusReason, pnum = options.pnum, psize = options.psize, noThrow = options.noThrow;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();
                        url = config.baseUrl + 'idevice/getIdeviceAvailabilityCount?';

                        if (deployed) url += '&deployed=' + deployed;
                        if (statusReasons.length) url += "&availability_status_reasons=" + statusReasons.join(",");
                        if (!pnum) pnum = 1;
                        if (!psize) psize = 20;

                        url += '&pnum=' + pnum + '&psize=' + psize;
                        _context.next = 11;
                        return (0, _get2.default)(url, { token: config.token });

                    case 11:
                        results = _context.sent;

                        if (!noThrow) {
                            _context.next = 14;
                            break;
                        }

                        return _context.abrupt('return', results);

                    case 14:
                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 16;
                            break;
                        }

                        throw results;

                    case 16:
                        return _context.abrupt('return', results);

                    case 19:
                        _context.prev = 19;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 22:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 19]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getAvailabilityCount.api.js.map