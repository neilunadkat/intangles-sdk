'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _put = require('../../../helpers/put');

var _put2 = _interopRequireDefault(_put);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var url, file, results;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        url = options.url, file = options.file;
                        _context.prev = 1;
                        _context.next = 4;
                        return (0, _put2.default)(url, file);

                    case 4:
                        results = _context.sent;

                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 7;
                            break;
                        }

                        throw results;

                    case 7:
                        return _context.abrupt('return', results);

                    case 10:
                        _context.prev = 10;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 13:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 10]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=uploadCodeFile.js.map