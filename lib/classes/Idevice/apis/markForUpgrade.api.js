'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _post = require('../../../helpers/post');

var _post2 = _interopRequireDefault(_post);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var id, body, fotaV2, config, url, results;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        id = options.id, body = options.body, fotaV2 = options.fotaV2;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();

                        if (config.baseUrl.substr(-1) != '/') config.baseUrl += "/";
                        url = config.baseUrl + ('idevice/' + id + '/upgradeV2?');

                        if (fotaV2) url += '&fota_v2=' + fotaV2;
                        _context.next = 8;
                        return (0, _post2.default)(url, { body: body, token: config.token, accountId: config.accountId });

                    case 8:
                        results = _context.sent;

                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 11;
                            break;
                        }

                        throw results;

                    case 11:
                        return _context.abrupt('return', results);

                    case 14:
                        _context.prev = 14;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 17:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 14]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=markForUpgrade.api.js.map