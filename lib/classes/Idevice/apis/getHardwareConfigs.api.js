'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require('../../../helpers/get');

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var generation, version, query, pnum, psize, is_aux, model_name, model_id, ids, noThrow, config, url, results;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        generation = options.generation, version = options.version, query = options.query, pnum = options.pnum, psize = options.psize, is_aux = options.is_aux, model_name = options.model_name, model_id = options.model_id, ids = options.ids, noThrow = options.noThrow;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();
                        url = config.baseUrl + 'idevice/hwconfig/list?';

                        generation ? url += '&generation=' + generation : null;
                        version ? url += '&version=' + version : null;
                        query ? url += '&query=' + query : null;
                        is_aux ? url += '&is_aux=' + is_aux : null;
                        model_name ? url += '&model_name=' + model_name : null;
                        model_id ? url += '&model_id=' + model_id : null;
                        if (Array.isArray(ids)) url = url + "&ids=" + ids.join(",");else if (ids) url = url + "&ids=" + ids;
                        if (!pnum) pnum = 1;
                        if (!psize) psize = 20;

                        url += '&pnum=' + pnum + '&psize=' + psize;
                        _context.next = 16;
                        return (0, _get2.default)(url, { token: config.token });

                    case 16:
                        results = _context.sent;

                        if (!noThrow) {
                            _context.next = 19;
                            break;
                        }

                        return _context.abrupt('return', results);

                    case 19:
                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 21;
                            break;
                        }

                        throw results;

                    case 21:
                        return _context.abrupt('return', results);

                    case 24:
                        _context.prev = 24;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 27:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 24]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getHardwareConfigs.api.js.map