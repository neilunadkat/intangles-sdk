"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require("../../../helpers/get");

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require("../../../helpers/fetchConfig");

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var noThrow, query, pnum, psize, showall, getVehicleInfo, vehicleId, imeis, imei, accountIds, vids, vid, type, tv, l2, ov, stm, tags, manufacturingBatches, isAvailable, lotId, deployed, deviceModel, deviceVersion, auxBoard, statusReasons, hwconfigDefinition, config, url, results;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        noThrow = options.noThrow, query = options.query, pnum = options.pnum, psize = options.psize, showall = options.showall, getVehicleInfo = options.getVehicleInfo, vehicleId = options.vehicleId, imeis = options.imeis, imei = options.imei, accountIds = options.accountIds, vids = options.vids, vid = options.vid, type = options.type, tv = options.tv, l2 = options.l2, ov = options.ov, stm = options.stm, tags = options.tags, manufacturingBatches = options.manufacturingBatches, isAvailable = options.isAvailable, lotId = options.lotId, deployed = options.deployed, deviceModel = options.deviceModel, deviceVersion = options.deviceVersion, auxBoard = options.auxBoard, statusReasons = options.statusReasons, hwconfigDefinition = options.hwconfigDefinition;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();
                        url = config.baseUrl + "idevice/listV2?";

                        if (query && query != "*") url += "&query=" + query;
                        if (showall) url += "&showall=true";
                        if (getVehicleInfo) url += "&getvehicleInfo=true";
                        if (vehicleId) url = url + "&vid=" + vehicleId;
                        if (Array.isArray(imeis)) url = url + "&imeis=" + imeis.join(",");else if (imeis) url = url + "&imei=" + imeis;
                        if (imei) url = url + "&imei=" + imei;
                        if (!pnum) pnum = 1;
                        if (!psize) psize = 20;
                        if (Array.isArray(accountIds) && accountIds.length) url += "&acc_id=" + accountIds.join(",");else if (typeof accountIds === "string") url += "&acc_id=" + accountIds;

                        if (Array.isArray(vids) && vids.length) url += "&vids=" + vids.join(",");else if (typeof vids === "string") url += "&vids=" + vids;
                        if (type) url = url + "&type=" + type;

                        if (vid) url = url + "&vid=" + vid;

                        if (lotId) url = url + "&lot_id=" + lotId;

                        if (Array.isArray(tv) && tv.length) url += "&tv=" + tv.join(",");else if (typeof tv === "string") url += "&tv=" + tv;

                        if (Array.isArray(l2) && l2.length) url += "&l2=" + l2.join(",");else if (typeof l2 === "string") url += "&l2=" + l2;

                        if (Array.isArray(ov) && ov.length) url += "&ov=" + ov.join(",");else if (typeof ov === "string") url += "&ov=" + ov;

                        if (Array.isArray(stm) && stm.length) url += "&stm=" + stm.join(",");else if (typeof stm === "string") url += "&stm=" + stm;

                        if (Array.isArray(statusReasons) && statusReasons.length) url += "&availability_status_reasons=" + statusReasons.join(",");else if (typeof statusReasons === "string") url += "&availability_status_reasons=" + statusReasons;

                        if (Array.isArray(manufacturingBatches) && manufacturingBatches.length) url += "&manufacturing_batches=" + manufacturingBatches.join(",");else if (typeof manufacturingBatches === "string") url += "&manufacturing_batches=" + manufacturingBatches;

                        if (Array.isArray(deviceModel) && deviceModel.length) url += "&device_model=" + deviceModel.join(",");else if (typeof deviceModel === "string") url += "&device_model=" + deviceModel;

                        if (Array.isArray(deviceVersion) && deviceVersion.length) url += "&device_version=" + deviceVersion.join(",");else if (typeof deviceVersion === "string") url += "&device_version=" + deviceVersion;

                        if (Array.isArray(auxBoard) && auxBoard.length) url += "&aux_board=" + auxBoard.join(",");else if (typeof auxBoard === "string") url += "&aux_board=" + auxBoard;

                        if (Array.isArray(tags) && tags.length) url += "&aux_board=" + tags.join(",");else if (typeof tags === "string") url += "&aux_board=" + tags;

                        if (deployed) url += '&deployed=true';

                        if (isAvailable) url += '&is_available=true';
                        if (hwconfigDefinition) url += '&hwconfig_definition=true';

                        url += "&pnum=" + pnum + "&psize=" + psize;
                        _context.next = 33;
                        return (0, _get2.default)(url, { token: config.token });

                    case 33:
                        results = _context.sent;

                        if (!noThrow) {
                            _context.next = 36;
                            break;
                        }

                        return _context.abrupt("return", results);

                    case 36:
                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 38;
                            break;
                        }

                        throw results;

                    case 38:
                        return _context.abrupt("return", results);

                    case 41:
                        _context.prev = 41;
                        _context.t0 = _context["catch"](1);
                        throw _context.t0;

                    case 44:
                    case "end":
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 41]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getIdeviceList.api.js.map