'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _get = require('../../../helpers/get');

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var token, _options$psize, psize, _options$pnum, pnum, status, query, stage, baseUrl, accountId, lastLoc, obd, sorting, updatedAfter, config, url, sort, i, sortType, msort, results;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        token = options.token, _options$psize = options.psize, psize = _options$psize === undefined ? 50 : _options$psize, _options$pnum = options.pnum, pnum = _options$pnum === undefined ? 1 : _options$pnum, status = options.status, query = options.query, stage = options.stage, baseUrl = options.baseUrl, accountId = options.accountId, lastLoc = options.lastLoc, obd = options.obd, sorting = options.sorting, updatedAfter = options.updatedAfter;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();
                        url = config.baseUrl + "vehicle/getlist?pnum=" + pnum + "&psize=" + psize;

                        if ((typeof query === 'undefined' ? 'undefined' : _typeof(query)) === 'object') {
                            if (query.vid) url += "&vid=" + query.vid;
                            if (query.query) url += "&query=" + query.query;
                        } else if (typeof query === 'string') {
                            url += "&query=" + query;
                        }
                        if (updatedAfter) url = url + "&updatedafter=" + updatedAfter;
                        if (lastLoc) url = url + "&lastloc=true&drivers=true";else url = url + "&lastloc=false";
                        if (obd) {
                            url = url + "&obd=true";
                            url = url + "&sort=health desc";
                        }
                        if (sorting) {
                            sort = '';

                            for (i = 0; i < sorting.length; i++) {
                                sortType = sorting[i].isasc ? 'asc' : 'desc';

                                sort += sorting[i].property + ' ' + sortType;
                            }
                            if (sort && sort !== '') {
                                url += "&sort=" + sort;
                            }
                        }if (options) {
                            if (options.fuel) {
                                url += "&fuel=" + options.fuel;
                            }
                            if (options.msort) {
                                msort = '';

                                for (i = 0; i < options.msort.length; i++) {
                                    sortType = options.msort[i].isasc ? 'asc' : 'desc';

                                    msort += options.msort[i].property + ' ' + sortType;
                                }
                                if (msort && msort !== '') {
                                    url += "&msort=" + msort;
                                }
                            }
                            if (options.healths) url += "&healths=" + options.healths;
                            if (options.spec_ids) url += "&spec_ids=" + options.spec_ids;
                            if (options.specs) url += "&specs=" + options.specs;
                            if (options.adblue) url += "&adblue=" + options.adblue;
                            if (options.moving) url += "&moving=" + options.moving;
                            if (options.algos_only) url += "&algos_only=" + options.algos_only;
                            if (options.obd_odo) url += "&obd_odo=true";
                            if (options.algo_issues) url += "&algo_issues=" + options.algo_issues;
                        }

                        _context.next = 12;
                        return (0, _get2.default)(url, { token: config.token, accountId: accountId || config.accountId });

                    case 12:
                        results = _context.sent;

                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 15;
                            break;
                        }

                        throw results;

                    case 15:
                        return _context.abrupt('return', results);

                    case 18:
                        _context.prev = 18;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 21:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 18]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getVehicleList.api.js.map