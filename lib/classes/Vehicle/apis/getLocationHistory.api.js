'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require('../../../helpers/get');

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var token, baseUrl, accountId, vehicleId, startTime, endTime, alertLog, callback, _options$noTotal, noTotal, psize, _options$pnum, pnum, config, url, results;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        token = options.token, baseUrl = options.baseUrl, accountId = options.accountId, vehicleId = options.vehicleId, startTime = options.startTime, endTime = options.endTime, alertLog = options.alertLog, callback = options.callback, _options$noTotal = options.noTotal, noTotal = _options$noTotal === undefined ? true : _options$noTotal, psize = options.psize, _options$pnum = options.pnum, pnum = _options$pnum === undefined ? 1 : _options$pnum;
                        _context.prev = 1;

                        if (vehicleId) {
                            _context.next = 4;
                            break;
                        }

                        throw new Error("Please provide vehicle id");

                    case 4:
                        config = _fetchConfig2.default.bind(this, options, true)();
                        url = config.baseUrl + ('vehicle/' + vehicleId + '/history/' + startTime + '/' + endTime + '?');

                        if (alertLog) url += "&alertlog=true";
                        if (callback) url += "&callback=true";
                        if (noTotal) url += "&no_total=true";
                        if (!pnum) pnum = 1;
                        url += '&pnum=' + pnum;
                        if (psize) url += '&psize=' + psize;
                        _context.next = 14;
                        return (0, _get2.default)(url, { token: config.token, accountId: config.accountId });

                    case 14:
                        results = _context.sent;
                        return _context.abrupt('return', results);

                    case 18:
                        _context.prev = 18;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 21:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 18]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getLocationHistory.api.js.map