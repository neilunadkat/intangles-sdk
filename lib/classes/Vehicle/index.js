'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Intangles = require('../Intangles');

var _Intangles2 = _interopRequireDefault(_Intangles);

var _localstorage = require('../../helpers/localstorage');

var _localstorage2 = _interopRequireDefault(_localstorage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Vehicle = function (_IntanglesObject) {
    _inherits(Vehicle, _IntanglesObject);

    _createClass(Vehicle, [{
        key: 'setConfig',
        value: function setConfig() {
            var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

            if (config.baseUrl) this.baseUrl = config.baseUrl;
            if (config.token) this.token = config.token;
            if (config.storage) this.storage = config.storage;else if (window && window.localStorage) this.storage = _localstorage2.default;
        }
    }]);

    function Vehicle() {
        var _ref2;

        var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
            baseUrl = _ref.baseUrl,
            token = _ref.token,
            storage = _ref.storage;

        _classCallCheck(this, Vehicle);

        var _this = _possibleConstructorReturn(this, (Vehicle.__proto__ || Object.getPrototypeOf(Vehicle)).call(this, (_ref2 = {}, baseUrl = _ref2.baseUrl, token = _ref2.token, storage = _ref2.storage, _ref2)));

        _this.getVehicleList = require('./apis/getVehicleList.api.js').default.bind(_this) || require('./apis/getVehicleList.api.js').bind(_this);
        _this.getVehicleInfo = require('./apis/getVehicleInfo.api.js').default.bind(_this) || require('./apis/getVehicleInfo.api.js').bind(_this);
        _this.getHealthAlgoResults = require('./apis/getHealthAlgoResults.api.js').default.bind(_this) || require('./apis/getHealthAlgoResults.api.js').bind(_this);

        return _this;
    }

    return Vehicle;
}(_Intangles2.default);

exports.default = Vehicle;
//# sourceMappingURL=index.js.map