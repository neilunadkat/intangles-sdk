'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _post = require('../../../helpers/post');

var _post2 = _interopRequireDefault(_post);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var psize, pnum, specs, manufacturer, account_id, query, model, config, url, results;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        psize = options.psize, pnum = options.pnum, specs = options.specs, manufacturer = options.manufacturer, account_id = options.account_id, query = options.query, model = options.model;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();

                        if (config.baseUrl.substr(-1) != '/') config.baseUrl += "/";
                        url = config.baseUrl + "specs/listV2?";

                        psize ? url += '&psize={psize}' : null;
                        pnum ? url += '&pnum={pnum}' : null;
                        specs ? url += '&specs={specs}' : null;
                        manufacturer ? url += '&manufacturer={manufacturer}' : null;
                        account_id ? url += '&account_id={account_id}' : null;
                        query ? url += '&query={query}' : null;
                        model ? url += '&model={model}' : null;

                        _context.next = 14;
                        return (0, _post2.default)(url, { token: config.token, accountId: config.accountId });

                    case 14:
                        results = _context.sent;

                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 17;
                            break;
                        }

                        throw results;

                    case 17:
                        return _context.abrupt('return', results);

                    case 20:
                        _context.prev = 20;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 23:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 20]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=listSpec.api.js.map