'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _get = require('../../../helpers/get');

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

var _getAccounts = require('./getAccounts.api');

var _getAccounts2 = _interopRequireDefault(_getAccounts);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var token, baseUrl, accountId, noCache, config, results, account, url;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        token = options.token, baseUrl = options.baseUrl, accountId = options.accountId, noCache = options.noCache;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options, true)();

                        if (!noCache) {
                            _context.next = 11;
                            break;
                        }

                        _context.next = 6;
                        return (0, _getAccounts2.default)(_extends({ baseUrl: config.baseUrl, token: config.token }, options, { accIds: [accountId] }));

                    case 6:
                        results = _context.sent;

                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 9;
                            break;
                        }

                        throw results;

                    case 9:
                        account = results.accounts && results.accounts.length ? results.accounts[0] : {};
                        return _context.abrupt('return', {
                            paging: results.paging,
                            status: results.status,
                            result: account
                        });

                    case 11:
                        url = config.baseUrl + ('accountV2/' + config.accountId + '?');
                        _context.next = 14;
                        return (0, _get2.default)(url, { token: config.token, accountId: config.accountId });

                    case 14:
                        results = _context.sent;

                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 17;
                            break;
                        }

                        throw results;

                    case 17:
                        return _context.abrupt('return', results);

                    case 20:
                        _context.prev = 20;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 23:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 20]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getAccountInfo.api.js.map