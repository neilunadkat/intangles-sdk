'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getVehicleCountBasedOnHealth = require('../../Vehicle/apis/getVehicleCountBasedOnHealth.api');

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_getVehicleCountBasedOnHealth).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
//# sourceMappingURL=getVehicleCountBasedOnHealth.api.js.map