'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getVehicleList = require('../../Vehicle/apis/getVehicleList.api');

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_getVehicleList).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
//# sourceMappingURL=getVehicleList.api.js.map