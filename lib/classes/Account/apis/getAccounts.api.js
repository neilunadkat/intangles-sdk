'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require('../../../helpers/get');

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var token, _options$psize, psize, _options$pnum, pnum, status, query, stage, baseUrl, accountId, _options$accIds, accIds, typeFilter, getRates, zohoinfo, config, url, results;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        token = options.token, _options$psize = options.psize, psize = _options$psize === undefined ? 50 : _options$psize, _options$pnum = options.pnum, pnum = _options$pnum === undefined ? 1 : _options$pnum, status = options.status, query = options.query, stage = options.stage, baseUrl = options.baseUrl, accountId = options.accountId, _options$accIds = options.accIds, accIds = _options$accIds === undefined ? [] : _options$accIds, typeFilter = options.typeFilter, getRates = options.getRates, zohoinfo = options.zohoinfo;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();
                        url = config.baseUrl + "account/listV2?";

                        if (psize) url += "&psize=" + psize;
                        if (accIds) url += "&acc_ids=" + accIds.join(',');
                        if (pnum) url += "&pnum=" + pnum;
                        if (status) url += "&status=" + status;else url += "&status=*";
                        if (query) url += "&query=" + query;else url += "&query=*";
                        if (stage && stage != 'ALL') url += "&stage=" + stage;
                        if (typeFilter) url += "&type=" + typeFilter;
                        if (getRates) url += "&getrates=true";
                        if (zohoinfo) url += "&zohoinfo=" + zohoinfo;
                        _context.next = 15;
                        return (0, _get2.default)(url, { token: config.token });

                    case 15:
                        results = _context.sent;

                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 18;
                            break;
                        }

                        throw results;

                    case 18:
                        return _context.abrupt('return', results);

                    case 21:
                        _context.prev = 21;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 24:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 21]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getAccounts.api.js.map