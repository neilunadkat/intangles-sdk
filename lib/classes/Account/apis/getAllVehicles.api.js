'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _getVehicleList = require('./getVehicleList.api');

var _getVehicleList2 = _interopRequireDefault(_getVehicleList);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var data, res, total;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        data = [];

                        options.pnum = 1;
                        options.psize = 200;

                    case 4:
                        _context.next = 6;
                        return _getVehicleList2.default.bind(this)(options);

                    case 6:
                        res = _context.sent;

                        if (!(!res || !res.status || res.status.code != 200)) {
                            _context.next = 9;
                            break;
                        }

                        throw res;

                    case 9:
                        total = res.paging.total;

                        data = data.concat(res.v);
                        options.pnum++;

                        if (!(total <= data.length)) {
                            _context.next = 14;
                            break;
                        }

                        return _context.abrupt('return', {
                            total: total,
                            v: data
                        });

                    case 14:
                        if (total > data.length) {
                            _context.next = 4;
                            break;
                        }

                    case 15:
                        _context.next = 20;
                        break;

                    case 17:
                        _context.prev = 17;
                        _context.t0 = _context['catch'](0);
                        throw _context.t0;

                    case 20:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 17]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getAllVehicles.api.js.map