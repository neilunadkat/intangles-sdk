'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require('../../../helpers/get');

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var token, accountId, psize, pnum, from, to, vehicles, _options$types, types, baseUrl, config, url, results;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        token = options.token, accountId = options.accountId, psize = options.psize, pnum = options.pnum, from = options.from, to = options.to, vehicles = options.vehicles, _options$types = options.types, types = _options$types === undefined ? ['over_speed'] : _options$types, baseUrl = options.baseUrl;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options, true)();
                        url = config.baseUrl + ('alertlog/logsV2/' + from + '/' + to + '?');

                        if (psize) url += "&psize=" + psize;else url += "&psize=" + 20;
                        if (pnum) url += "&pnum=" + pnum;else url += "&pnum=" + 1;
                        if (types) url += "&types=" + types.join(',');
                        if (vehicles && vehicles.length > 0) url += vehicles instanceof Array ? "&vehicles=" + vehicles.join(',') : "&vehicles=" + vehicles;
                        _context.next = 10;
                        return (0, _get2.default)(url, { token: config.token, accountId: config.accountId });

                    case 10:
                        results = _context.sent;

                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 13;
                            break;
                        }

                        throw results;

                    case 13:
                        return _context.abrupt('return', results);

                    case 16:
                        _context.prev = 16;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 19:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 16]]);
    }));

    function fetchAlerts() {
        return _ref.apply(this, arguments);
    }

    return fetchAlerts;
}();
//# sourceMappingURL=getAlertLogs.api.js.map