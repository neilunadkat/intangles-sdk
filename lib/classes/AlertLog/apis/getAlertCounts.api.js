'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require('../../../helpers/get');

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var token, accountId, _options$psize, psize, _options$pnum, pnum, vehicles, from, to, baseUrl, config, url, types, results;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        token = options.token, accountId = options.accountId, _options$psize = options.psize, psize = _options$psize === undefined ? 20 : _options$psize, _options$pnum = options.pnum, pnum = _options$pnum === undefined ? 1 : _options$pnum, vehicles = options.vehicles, from = options.from, to = options.to, baseUrl = options.baseUrl;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options, true)();
                        url = config.baseUrl + ('alertlog/logsV2/' + from + '/' + to + '/count?');
                        types = ['over_speed', 'speeding', 'idling', 'stoppage', 'hard_brake', 'freerun'];

                        if (psize) url += "&psize=" + psize;else url += "&psize=" + 20;
                        if (pnum) url += "&pnum=" + pnum;else url += "&pnum=" + 1;
                        if (types) url += "&types=" + types.join(',');
                        if (vehicles && vehicles.length > 0) url += vehicles instanceof Array ? "&vehicles=" + vehicles.join(',') : "&vehicles=" + vehicles;
                        _context.next = 11;
                        return (0, _get2.default)(url, { token: config.token, accountId: config.accountId });

                    case 11:
                        results = _context.sent;

                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 14;
                            break;
                        }

                        throw results;

                    case 14:
                        return _context.abrupt('return', results);

                    case 17:
                        _context.prev = 17;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 20:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 17]]);
    }));

    function fetchAlertCounts() {
        return _ref.apply(this, arguments);
    }

    return fetchAlertCounts;
}();
//# sourceMappingURL=getAlertCounts.api.js.map