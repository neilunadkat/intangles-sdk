'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function () {
    this.storage.removeItem('User');
    this.storage.removeItem('UserToken');
    this.storage.removeItem('UserTokenDate');
    this.setConfig({
        token: null,
        accountId: null
    });
};
//# sourceMappingURL=logout.api.js.map