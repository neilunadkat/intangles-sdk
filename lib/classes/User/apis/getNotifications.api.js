'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require('../../../helpers/get');

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var startTime, endTime, notificationTypes, subtypes, types, vehicles, pnum, psize, users, noThrow, accountFilter, not_id, notificationId, pushOpened, push_opened, config, url, results;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        startTime = options.startTime, endTime = options.endTime, notificationTypes = options.notificationTypes, subtypes = options.subtypes, types = options.types, vehicles = options.vehicles, pnum = options.pnum, psize = options.psize, users = options.users, noThrow = options.noThrow, accountFilter = options.accountFilter, not_id = options.not_id, notificationId = options.notificationId, pushOpened = options.pushOpened, push_opened = options.push_opened;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();
                        url = config.baseUrl + 'notification/get?';

                        if (accountFilter) url += 'account_id=' + accountFilter;
                        if (users && !Array.isArray(users)) users = [users];
                        if (notificationTypes && !Array.isArray(notificationTypes)) notificationTypes = [notificationTypes];
                        if (vehicles && !Array.isArray(vehicles)) vehicles = [vehicles];
                        if (types && !Array.isArray(types)) types = [types];
                        if (subtypes && !Array.isArray(subtypes)) subtypes = [subtypes];
                        if (!pnum) pnum = 1;
                        url += '&pnum=' + pnum;
                        if (psize) url += '&psize=' + psize;
                        if (users && users.length) url += '&user_id=' + users.join(',');
                        if (notificationTypes && notificationTypes.length) url += '&notification_type=' + notificationTypes.join(',');
                        if (subtypes && subtypes.length) url += '&subtype=' + subtypes.join(',');
                        if (types && types.length) url += '&type=' + types.join(',');
                        if (vehicles && vehicles.length) url += '&vehicle_id=' + vehicles.join(',');
                        if (endTime) url += '&end_time=' + endTime;
                        if (startTime) url += '&start_time=' + startTime;
                        if (pushOpened || push_opened) url += '&push_opened=true';
                        if (not_id) url += '&not_id=' + not_id;
                        if (notificationId) url += '&not_id=' + notificationId;
                        _context.next = 25;
                        return (0, _get2.default)(url, { token: config.token, accountId: config.accountId });

                    case 25:
                        results = _context.sent;

                        if (!noThrow) {
                            _context.next = 28;
                            break;
                        }

                        return _context.abrupt('return', results);

                    case 28:
                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 30;
                            break;
                        }

                        throw results;

                    case 30:
                        return _context.abrupt('return', results);

                    case 33:
                        _context.prev = 33;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 36:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 33]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getNotifications.api.js.map