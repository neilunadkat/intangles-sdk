'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _getUserByToken = require('./getUserByToken.api');

var _getUserByToken2 = _interopRequireDefault(_getUserByToken);

var _localstorage = require('../../../helpers/localstorage');

var _localstorage2 = _interopRequireDefault(_localstorage);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var givenOptions = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var config, options;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.prev = 0;
                        config = _fetchConfig2.default.bind(this, givenOptions)();
                        _context.next = 4;
                        return this.storage.getItem('UserToken');

                    case 4:
                        _context.t0 = _context.sent;

                        if (_context.t0) {
                            _context.next = 7;
                            break;
                        }

                        _context.t0 = config.token;

                    case 7:
                        _context.t1 = _context.t0;
                        _context.t2 = config.baseUrl;
                        options = {
                            token: _context.t1,
                            baseUrl: _context.t2
                        };

                        if (options.token) {
                            _context.next = 12;
                            break;
                        }

                        throw new Error("No token found");

                    case 12:
                        _context.next = 14;
                        return _getUserByToken2.default.bind(this, options)();

                    case 14:
                        _context.t3 = _context.sent;
                        return _context.abrupt('return', {
                            valid: true,
                            result: _context.t3
                        });

                    case 18:
                        _context.prev = 18;
                        _context.t4 = _context['catch'](0);

                        if (!(_context.t4 && _context.t4.status)) {
                            _context.next = 24;
                            break;
                        }

                        return _context.abrupt('return', {
                            valid: false,
                            error: _context.t4
                        });

                    case 24:
                        return _context.abrupt('return', {
                            valid: null,
                            error: _context.t4 && _context.t4.message || "Couldn't fetch"
                        });

                    case 25:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[0, 18]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=authenticate.api.js.map