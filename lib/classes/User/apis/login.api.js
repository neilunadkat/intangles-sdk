'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _post = require('../../../helpers/post');

var _post2 = _interopRequireDefault(_post);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var username, pwd, baseUrl, config, body, url, results, configObj;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        username = options.username, pwd = options.pwd, baseUrl = options.baseUrl;
                        _context.prev = 1;
                        config = {
                            baseUrl: baseUrl || (this && this.baseUrl ? this.baseUrl : 'https://apis.intangles.com/')
                        };

                        if (config.baseUrl.substr(-1) != '/') config.baseUrl += "/";
                        body = {
                            username: username,
                            pwd: pwd
                        };
                        url = config.baseUrl + "v2/user/login";
                        _context.next = 8;
                        return (0, _post2.default)(url, { body: body });

                    case 8:
                        results = _context.sent;

                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 11;
                            break;
                        }

                        throw results;

                    case 11:
                        if (!(this && this.storage)) {
                            _context.next = 14;
                            break;
                        }

                        _context.next = 14;
                        return Promise.all([this.storage.setItem("User", JSON.stringify(results.user)), this.storage.setItem("UserToken", results.token), this.storage.setItem("UserTokenDate", JSON.stringify(new Date().getTime()))]);

                    case 14:
                        configObj = {
                            token: results.token
                        };

                        if (results.user && !results.user.is_intangles_user && results.accountId) configObj.accountId = results.accountId;
                        if (this && this.setConfig) this.setConfig(configObj);
                        return _context.abrupt('return', results);

                    case 20:
                        _context.prev = 20;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 23:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 20]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=login.api.js.map