'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require('../../../helpers/get');

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var _options$psize, psize, _options$pnum, pnum, query, intanglesUsers, isEnabled, isIntanglesUser, isAccountAdmin, _options$noTotal, noTotal, config, url, results;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _options$psize = options.psize, psize = _options$psize === undefined ? 50 : _options$psize, _options$pnum = options.pnum, pnum = _options$pnum === undefined ? 1 : _options$pnum, query = options.query, intanglesUsers = options.intanglesUsers, isEnabled = options.isEnabled, isIntanglesUser = options.isIntanglesUser, isAccountAdmin = options.isAccountAdmin, _options$noTotal = options.noTotal, noTotal = _options$noTotal === undefined ? true : _options$noTotal;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();
                        url = config.baseUrl + "v2/user/list?pnum=" + pnum + "&psize=" + psize;

                        if (query) url += "&query=" + query;
                        if (intanglesUsers) url += "&intangles_users=" + intanglesUsers;
                        if (isIntanglesUser) url += "&is_intangles_user=" + isIntanglesUser;
                        if (isAccountAdmin) url += "&is_account_admin=" + isAccountAdmin;
                        if (isEnabled) url += "&is_enabled=" + isEnabled;
                        // var no_account_id;
                        // if (options && options.no_account_id)
                        //     no_account_id = true;
                        if (noTotal) url = url + "&no_total=true";
                        _context.next = 12;
                        return (0, _get2.default)(url, { token: config.token, accountId: config.accountId });

                    case 12:
                        results = _context.sent;

                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 15;
                            break;
                        }

                        throw results;

                    case 15:
                        return _context.abrupt('return', results);

                    case 18:
                        _context.prev = 18;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 21:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 18]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getUserList.api.js.map