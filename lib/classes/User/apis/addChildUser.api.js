'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _post = require('../../../helpers/post');

var _post2 = _interopRequireDefault(_post);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

var _isSuccessful = require('../../../helpers/isSuccessful');

var _isSuccessful2 = _interopRequireDefault(_isSuccessful);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var body, baseUrl, doLogin, config, url, results, configObj;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        body = options.body, baseUrl = options.baseUrl, doLogin = options.doLogin;
                        _context.prev = 1;

                        if (body) {
                            _context.next = 4;
                            break;
                        }

                        throw new Error('No data provided');

                    case 4:
                        config = _fetchConfig2.default.bind(this, options)();

                        if (config.baseUrl.substr(-1) != '/') config.baseUrl += "/";
                        url = config.baseUrl + "v2/user/create_child_user?";

                        if (doLogin) url += '&doLogin=true';
                        _context.next = 10;
                        return (0, _post2.default)(url, { body: body, token: config.token });

                    case 10:
                        results = _context.sent;

                        if ((0, _isSuccessful2.default)(results)) {
                            _context.next = 13;
                            break;
                        }

                        throw results;

                    case 13:
                        if (!(this && this.storage && results.user && results.user.user)) {
                            _context.next = 19;
                            break;
                        }

                        _context.next = 16;
                        return Promise.all([this.storage.setItem("User", JSON.stringify(results.user.user)), this.storage.setItem("UserToken", results.user.token), this.storage.setItem("UserTokenDate", JSON.stringify(new Date().getTime()))]);

                    case 16:
                        configObj = {
                            token: results.token
                        };

                        if (results.user && results.user.user && !results.user.user.is_intangles_user && results.user.user.accountId) configObj.accountId = results.user.user.accountId;
                        if (this && this.setConfig) this.setConfig(configObj);

                    case 19:
                        return _context.abrupt('return', results);

                    case 22:
                        _context.prev = 22;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 25:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 22]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=addChildUser.api.js.map