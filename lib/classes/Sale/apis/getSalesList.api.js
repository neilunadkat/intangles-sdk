'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require('../../../helpers/get');

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var token, _options$psize, psize, _options$pnum, pnum, baseUrl, accountFilter, productIds, imeis, saleTag, salesPerson, getUserInfo, stage, currency, deploymentType, time_gt, startTime, time_lt, endTime, accountInfo, accountType, billingType, period, sorting, saleType, certTypes, replacementOrder, config, url, sort, i, sortType, results;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        token = options.token, _options$psize = options.psize, psize = _options$psize === undefined ? 50 : _options$psize, _options$pnum = options.pnum, pnum = _options$pnum === undefined ? 1 : _options$pnum, baseUrl = options.baseUrl, accountFilter = options.accountFilter, productIds = options.productIds, imeis = options.imeis, saleTag = options.saleTag, salesPerson = options.salesPerson, getUserInfo = options.getUserInfo, stage = options.stage, currency = options.currency, deploymentType = options.deploymentType, time_gt = options.time_gt, startTime = options.startTime, time_lt = options.time_lt, endTime = options.endTime, accountInfo = options.accountInfo, accountType = options.accountType, billingType = options.billingType, period = options.period, sorting = options.sorting, saleType = options.saleType, certTypes = options.certTypes, replacementOrder = options.replacementOrder;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();
                        url = config.baseUrl + "sale/list?pnum=" + pnum + "&psize=" + psize;


                        imeis && imeis.length ? url += '&imeis=' + options.imeis.join(",") : null;
                        saleTag ? url = url + "&tag=" + saleTag : null;
                        salesPerson ? url = url + "&psales_person=" + salesPerson : null;
                        productIds && productIds.length ? url = url + "&product_id=" + productIds.join(",") : null;
                        accountFilter ? url = url + "&account_id=" + accountFilter : null;
                        if (stage) {
                            if (Array.isArray(stage)) {
                                url += '&stage=' + stage.join(',');
                            } else url += '&stage=' + stage;
                        }
                        if (certTypes) {
                            if (Array.isArray(certTypes)) {
                                url += '&cert_types=' + certTypes.join(',');
                            } else url += '&cert_types=' + certTypes;
                        }
                        if (saleType) {
                            if (Array.isArray(saleType)) {
                                url += '&sale_type=' + saleType.join(',');
                            } else url += '&sale_type=' + saleType;
                        }
                        replacementOrder ? url += "&replacement_order=" + replacementOrder : null;
                        currency ? url = url + "&currency=" + currency : null;
                        deploymentType ? url = url + "&deployment_type=" + deploymentType : null;
                        startTime ? url = url + "&time_gte=" + startTime : null;
                        time_gt ? url = url + "&time_gt=" + time_gt : null;
                        time_lt ? url = url + "&time_lt=" + time_lt : null;
                        endTime ? url = url + "&time_lte=" + endTime : null;
                        accountType ? url = url + "&account_type=" + accountType : null;
                        billingType ? url = url + "&billing_type=" + billingType : null;
                        period ? url = url + "&period=" + period : null;
                        accountInfo ? url += "&acc_info=true" : null;
                        if (getUserInfo) url += '&get_user_info=true';

                        if (sorting) {
                            sort = '';

                            for (i = 0; i < sorting.length; i++) {
                                sortType = sorting[i].isasc ? 'isasc' : 'desc';

                                sort && sort !== "" ? sort += "," : null;
                                sort += sorting[i].property + ' ' + sortType;
                            }
                            if (sort && sort !== '') {
                                url += "&sort=" + sort;
                            }
                        }
                        if (options) {
                            if (options.proj && options.proj.length) {
                                url += '&proj=' + options.proj.join(",");
                            }
                            if (options.tags && options.tags.length) {
                                url += '&tags=' + options.tags.join(",");
                            }
                            if (options.hasOwnProperty("payment_completed")) {
                                url += '&payment_completed=' + options.paymentCompleted;
                            }
                        }

                        _context.next = 28;
                        return (0, _get2.default)(url, { token: config.token, accountId: config.accountId });

                    case 28:
                        results = _context.sent;

                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 31;
                            break;
                        }

                        throw results;

                    case 31:
                        return _context.abrupt('return', results);

                    case 34:
                        _context.prev = 34;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 37:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 34]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getSalesList.api.js.map