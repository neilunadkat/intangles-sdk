'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require('../../../helpers/get');

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var fields, seq_no, status, pnum, psize, material, ids, noThrow, config, url, results;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        fields = options.fields, seq_no = options.seq_no, status = options.status, pnum = options.pnum, psize = options.psize, material = options.material, ids = options.ids, noThrow = options.noThrow;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();
                        url = config.baseUrl + '/device/packaging/carton/list?';

                        fields ? url += '&fields=' + fields : null;
                        seq_no ? url += '&seq_no=' + seq_no : null;
                        status ? url += '&status=' + status : null;
                        from ? url += '&from=' + from : null;
                        to ? url += '&to=' + to : null;
                        material ? url += '&material=' + material : null;

                        if (!pnum) pnum = 1;
                        if (!psize) psize = 20;

                        url += '&pnum=' + pnum + '&psize=' + psize;
                        _context.next = 15;
                        return (0, _get2.default)(url, { token: config.token });

                    case 15:
                        results = _context.sent;

                        if (!noThrow) {
                            _context.next = 18;
                            break;
                        }

                        return _context.abrupt('return', results);

                    case 18:
                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 20;
                            break;
                        }

                        throw results;

                    case 20:
                        return _context.abrupt('return', results);

                    case 23:
                        _context.prev = 23;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 26:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 23]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getCartonsList.api.js.map