"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _localstorage = require("../helpers/localstorage");

var _localstorage2 = _interopRequireDefault(_localstorage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var IntanglesObject = function IntanglesObject(baseUrl, token, storage) {
    _classCallCheck(this, IntanglesObject);

    this.baseUrl = baseUrl;
    this.token = token;
    if (storage) this.storage = storage;else this.storage = _localstorage2.default;
};

exports.default = IntanglesObject;
//# sourceMappingURL=Intangles.js.map