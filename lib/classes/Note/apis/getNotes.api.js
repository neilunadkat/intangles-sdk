'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require('../../../helpers/get');

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var token, _options$psize, psize, _options$pnum, pnum, baseUrl, accountId, getUserInfo, getArchived, entityId, entityType, config, url, results;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        token = options.token, _options$psize = options.psize, psize = _options$psize === undefined ? 50 : _options$psize, _options$pnum = options.pnum, pnum = _options$pnum === undefined ? 1 : _options$pnum, baseUrl = options.baseUrl, accountId = options.accountId, getUserInfo = options.getUserInfo, getArchived = options.getArchived, entityId = options.entityId, entityType = options.entityType;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();
                        url = config.baseUrl + "note/list?";


                        if (accountId) url += '&account_id=' + accountId;
                        if (entityType) url += '&entity_type=' + entityType;
                        if (entityId) url += '&entity_id=' + entityId;
                        if (getUserInfo) url += '&get_user_info=true';
                        if (!getArchived) url += '&states=1,2,3';else url += '&states=*';
                        if (pnum) url += '&pnum=' + pnum;
                        if (psize) url += '&psize=' + psize;else url += '&psize=3';

                        _context.next = 13;
                        return (0, _get2.default)(url, { token: config.token, accountId: accountId || config.accountId });

                    case 13:
                        results = _context.sent;

                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 16;
                            break;
                        }

                        throw results;

                    case 16:
                        return _context.abrupt('return', results);

                    case 19:
                        _context.prev = 19;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 22:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 19]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getNotes.api.js.map