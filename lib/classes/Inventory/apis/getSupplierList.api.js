"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require("../../../helpers/get");

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require("../../../helpers/fetchConfig");

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var noThrow, type, pnum, psize, name, currency, fields, query, config, url, results;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        noThrow = options.noThrow, type = options.type, pnum = options.pnum, psize = options.psize, name = options.name, currency = options.currency, fields = options.fields, query = options.query;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();
                        url = config.baseUrl + "/inventory/supplier/list?";


                        if (!pnum) pnum = 1;
                        if (!psize) psize = 20;
                        if (type) url = url + "&type=" + type;
                        if (fields) url = url + "&fields=" + fields;
                        if (name) url = url + "&name=" + name;
                        if (currency) url = url + "&currency=" + currency;
                        if (query) url = url + "&query=" + query;

                        url += "&pnum=" + pnum + "&psize=" + psize;
                        _context.next = 14;
                        return (0, _get2.default)(url, { token: config.token });

                    case 14:
                        results = _context.sent;

                        if (!noThrow) {
                            _context.next = 17;
                            break;
                        }

                        return _context.abrupt("return", results);

                    case 17:
                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 19;
                            break;
                        }

                        throw results;

                    case 19:
                        return _context.abrupt("return", results);

                    case 22:
                        _context.prev = 22;
                        _context.t0 = _context["catch"](1);
                        throw _context.t0;

                    case 25:
                    case "end":
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 22]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getSupplierList.api.js.map