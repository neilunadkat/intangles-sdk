"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _get = require("../../../helpers/get");

var _get2 = _interopRequireDefault(_get);

var _fetchConfig = require("../../../helpers/fetchConfig");

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var noThrow, pnum, psize, fields, category, specification, intanglesPartCode, supplierPartCode, compClass, query, make, config, url, results;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        noThrow = options.noThrow, pnum = options.pnum, psize = options.psize, fields = options.fields, category = options.category, specification = options.specification, intanglesPartCode = options.intanglesPartCode, supplierPartCode = options.supplierPartCode, compClass = options.compClass, query = options.query, make = options.make;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();
                        url = config.baseUrl + "/inventory/hwcomponent/list?";


                        if (!pnum) pnum = 1;
                        if (!psize) psize = 20;
                        if (category) url = url + "&category=" + category;
                        if (fields) url = url + "&fields=" + fields;
                        if (specification) url = url + "&specification=" + specification;
                        if (intanglesPartCode) url = url + "&intangles_part_code=" + intanglesPartCode;
                        if (supplierPartCode) url = url + "&supplier_part_code=" + supplierPartCode;
                        if (compClass) url = url + "&class=" + compClass;
                        if (query) url = url + "&query=" + query;
                        if (make) url = url + "&make=" + make;

                        url += "&pnum=" + pnum + "&psize=" + psize;
                        _context.next = 17;
                        return (0, _get2.default)(url, { token: config.token });

                    case 17:
                        results = _context.sent;

                        if (!noThrow) {
                            _context.next = 20;
                            break;
                        }

                        return _context.abrupt("return", results);

                    case 20:
                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 22;
                            break;
                        }

                        throw results;

                    case 22:
                        return _context.abrupt("return", results);

                    case 25:
                        _context.prev = 25;
                        _context.t0 = _context["catch"](1);
                        throw _context.t0;

                    case 28:
                    case "end":
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 25]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=getComponentList.api.js.map