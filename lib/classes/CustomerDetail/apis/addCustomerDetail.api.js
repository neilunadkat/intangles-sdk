'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _post = require('../../../helpers/post');

var _post2 = _interopRequireDefault(_post);

var _fetchConfig = require('../../../helpers/fetchConfig');

var _fetchConfig2 = _interopRequireDefault(_fetchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; } /***
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Adds new customer detail
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * @param {Object} options Parameters to create new customer detail
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * @param {Object} options.body Body for post call
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * @param {String} options.body.name Name of the customer
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * @param {Number} options.body.phone_no Customer Phone no
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * @param {String} options.body.address Customer Address
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * @param {String} options.body.address_proof_no Customer address proof no
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * @param {String} options.body.identity_proof_no Customer identity proof no
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * @param {Boolean} options.body.files Customer other documents and files
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * @param {Object} options.body.address_proof Customer address proof
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * @param {Object} options.body.identity_proof Customer identity proof
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            */


//@flow
exports.default = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var body, config, url, results;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        body = options.body;
                        _context.prev = 1;
                        config = _fetchConfig2.default.bind(this, options)();

                        if (config.baseUrl.substr(-1) != '/') config.baseUrl += "/";
                        url = config.baseUrl + 'customer/detail/add?';
                        _context.next = 7;
                        return (0, _post2.default)(url, { body: body, token: config.token, accountId: config.accountId });

                    case 7:
                        results = _context.sent;

                        if (!(!results || !results.status || results.status.code != 200)) {
                            _context.next = 10;
                            break;
                        }

                        throw results;

                    case 10:
                        return _context.abrupt('return', results);

                    case 13:
                        _context.prev = 13;
                        _context.t0 = _context['catch'](1);
                        throw _context.t0;

                    case 16:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[1, 13]]);
    }));

    return function () {
        return _ref.apply(this, arguments);
    };
}();
//# sourceMappingURL=addCustomerDetail.api.js.map