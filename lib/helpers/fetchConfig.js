'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function () {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var accountIdRequired = arguments[1];
    var token = options.token,
        baseUrl = options.baseUrl,
        accountId = options.accountId;

    var config = {
        baseUrl: baseUrl || (this && this.baseUrl ? this.baseUrl : 'https://apis.intangles.com/'),
        token: token || (this && this.token ? this.token : ''),
        accountId: accountId || (this && this.accountId ? this.accountId : '')
    };
    if (config.baseUrl.substr(-1) != '/') config.baseUrl += "/";
    if (accountIdRequired && !config.accountId) throw new Error("Please provide account id");
    return config;
};
//# sourceMappingURL=fetchConfig.js.map