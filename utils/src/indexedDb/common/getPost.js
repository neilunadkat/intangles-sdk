import fetch from 'isomorphic-fetch';
export default {
    get : async function(url){
        var response = await fetch(url,{
            headers: {
                "Content-Type":"application/json",
                "Accept":"application/json"
            }
        });
        var results = await response.json();
        return results;
    },
    post : async function(url){
        var response = await fetch(url,{
            method: "POST",
            headers: {
                "Content-Type":"application/json",
                "Accept":"application/json"
            }
        });
        var results = await response.json();
        return results;
    }
}