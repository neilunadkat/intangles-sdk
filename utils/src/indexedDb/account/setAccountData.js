import db from 'shared/db';
import getToken from 'shared/helpers/getToken';
import getSpecsFromLocal from './specs';
import fetchAllVehicles from 'shared/apis/fetchAllVehicles';
import fetchSpecs from 'shared/apis/fetchSpecs';
export default async function (accountId) {
    try {
        if (db == undefined) {
            // retrive live specs
            return await fetchSpecs(accountId)
        }
        var appconfig = {
            authToken: getToken(),
            current_account_id: accountId
        }
        await db.open();
        db.accounts.where("id").notEqual(accountId).modify(function (acc) {
            acc.current = 0;
        }, function (error) {
            console.log(error);
        });
        var iDbVehicles = await db.vehicles.where({ account_id: accountId }).toArray();
        if (!iDbVehicles || iDbVehicles.length == 0) {
            var fetchedVehicles = await fetchAllVehicles(accountId);
            var lastPutKey = await db.vehicles.bulkPut(fetchedVehicles);
            var updatedCurrentAccount = db.accounts.put({
                id: accountId,
                current: 1,
                last_updated: new Date()
            });
            console.log("Added or Updated");
        }
        else {
            db.accounts.where("id").equals(accountId).modify({
                current: 1
            });
            console.log("Using vehicles from localstorage");
        }
        await db.appconfig.clear();
        db.appconfig.add({
            current_account_id: accountId,
            token: getToken()
        }).catch(function(error){
            console.log(error);
        });
        var accountToSpecs = await db.accountToSpecs.where({ id: accountId }).toArray();
        if (!accountToSpecs || accountToSpecs.length == 0) {
            var specs = await fetchSpecs(accountId);
            if (specs) {
                var lastSpecPutKey = await db.specs.bulkPut(specs);
                await db.close();
                return specs;
            } else {
                await db.close();
                return;
            }
        }
        else {
            console.log("Using specs from localstorage");
            var res = await getSpecsFromLocal(accountId);
            await db.close();            
            return {
                specs: res
            }
    
        }
    }
    catch (e) {
        return await fetchSpecs(accountId)
    }
}