var fetchAllVehicles = require("store/common/fetchAllVehicles");
var fetchSpecs = require("store/common/fetchSpecs");
// Vehicle list exists in db.vehicles
var getSpecsFromLocal = require("./specs");
import db from '../db';
import getToken from 'shared/helpers/getToken';
export default async function(accountId)
{
    var appconfig = {
        authToken: getToken(),
        current_account_id: accountId
    }
    await db.open();
    if(db == undefined){
     // retrive live specs
       return fetchSpecs(appconfig,accountId,db)       
    }

    return new Promise(function(resolve,reject){
        db.accounts.where("id").notEqual(accountId).modify(function(acc){
            acc.current = 0;
        },function(error){
            console.log(error);
        });
        db.vehicles.where({account_id:accountId}).toArray(function(res)
        {
            if(!res || res.length==0)
            {
                fetchAllVehicles(appconfig,accountId).then(function(list){
                    db.vehicles.bulkPut(list).then(function(lastKey){
                        console.log(lastKey);
                        db.accounts.put({
                            id: accountId,
                            current: 1,
                            last_updated:new Date()
                        }).then(function(done){
                            console.log("Added or Updated");
                        }).catch(function(error) {
                            console.log('error',error);
                        });
                    })
                });
            }
            else
            {
                db.accounts.where("id").equals(accountId).modify({
                    current:1
                });
                console.log("Using vehicles from localstorage");
            } 
        },function(error){
            console.log(error);
        });
        db.appconfig.clear().then(function(){
           checkCurrent();
        }).catch(function(){
            checkCurrent();            
        })

        function checkCurrent() {
            if(!appconfig.current_account_id){
                db.appconfig.add({
                    current_account_id: accountId,
                    token: appconfig.authToken
                }).catch(function(error){
                    console.log(error);
                });
            }else{
                db.appconfig.add({
                    current_account_id:appconfig.current_account_id,
                    token: appconfig.authToken
                }).catch(function(error){
                    console.log(error);
                });
            }
        }
        db.accountToSpecs.where({id:accountId}).toArray((function(res){
            if(!res || res.length==0)
            {
                fetchSpecs(appconfig,accountId,db).then(function(specs){
                    if(specs){
                        db.specs.bulkPut(specs).then(function(lastKey){
                            resolve(specs);
                        })
                    }else{
                        resolve();
                    }                                       
                })
            }
            else
            {
                console.log("Using specs from localstorage");
                getSpecsFromLocal(db,account).then(function(res){
                    resolve({specs:res});
                })
                
            }
        }),function(error){
            console.log(error);
        });
    })    
};
