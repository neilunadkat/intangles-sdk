import db from 'shared/db';
export default async function(accountId){
    var data = await db.accountToSpecs.where({id:accountId}).first();
    if (data) {
        var specs = await db.specs.where("id").anyOf(data.specIds).toArray();
        return specs;
    }
    else
        return;
}