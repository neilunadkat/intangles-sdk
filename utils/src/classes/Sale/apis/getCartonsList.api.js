import get from '../../../helpers/get';
import fetchConfig from '../../../helpers/fetchConfig';
export default async function (options = {}) {
    var {
        fields,
        seq_no,
        status,
        pnum,
        psize,
        material,
        ids,
        noThrow
    } = options;
    try {
        var config = fetchConfig.bind(this, options)();
        var url = config.baseUrl + `/device/packaging/carton/list?`;
        fields ? url += '&fields=' + fields : null;
        seq_no ? url += '&seq_no=' + seq_no : null;
        status ? url += '&status=' + status : null;
        from ? url += '&from=' + from : null;
        to ? url += '&to=' + to : null;
        material ? url += '&material=' + material : null;

        if (!pnum)
            pnum = 1;
        if (!psize)
            psize = 20;

        url += `&pnum=${pnum}&psize=${psize}`;
        var results = await get(url, { token: config.token });
        if (noThrow)
            return results;
        if (!results || !results.status || results.status.code != 200)
            throw results;
        return results;
    }
    catch (e) {
        throw e;
    }
}