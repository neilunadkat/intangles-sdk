module.exports = function() {
var apis = require('./apis.json');
var program = require('commander');
var fs = require("fs");
program
  .version('0.1.0')
  .option('-d, --db <db>', 'Local storage')
  .parse(process.argv);
var db = program.db;
var indexFiles = `import localstorage from './helpers/localstorage';
`;
indexFiles += `export default class{
    constructor(config) {
        this.storage = localstorage;
        this.initialize(config);
    }
  	initialize(config) {
        this.setConfig(config);
    	##initializingText##
    }
 	
    setConfig(config={}) {
        if(!config)
            return;
        if(config.storage)
            this.storage = config.storage;
        if(config.baseUrl)
            this.baseUrl = config.baseUrl;
        if(config.accountId)
            this.accountId = config.accountId;
        if(config.token)
            this.token = config.token;
        else {
            this.storage.getItem("UserToken").then((token)=>{
                this.token = token;
            });
        }
    }

    async getCurrentUser() {
        var user = await this.storage.getItem("User");
        if(user)
            return JSON.parse(user);
        return null;
    }
    
}
`;
var txt = ``;
apis.forEach((item) => {
    txt += `this.${item.name}= {
`
    item.apis.forEach((api) => {
        var file = "./classes/"+item.className+"/apis/" + api.fileName + ".api.js";
        var apis = 'apis';
        var indexedDb = 'indexedDb';
        if(db == 'indexedDb' && api.local) {
            file = "./classes/"+item.className+"./indexedDb/" + api.fileName +".db.js";
        } 
        txt += `        ${api.name}: require('${file}').default.bind(this) || require('${file}').bind(this),
`
    });
    txt += `}
`;
})
indexFiles = indexFiles.replace("##initializingText##",txt);
fs.writeFileSync("./utils/src/index.js",indexFiles);
}