import getAllVehicles from '../../../../src/classes/Account/apis/getAllVehicles.api';
describe("Get all Vehicles api", () => {
    beforeEach(()=>{
        fetch.resetMocks();
    });
    
    it("should not throw error if empty options",async () => {
        fetch.once(JSON.stringify({
            status: {
                code: 200
            },
            paging: {
                total: 0
            },
            v: []
        }));
        var data =await getAllVehicles({ accountId: "sample"}); 
        expect(data).toBeDefined();
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/vehicle/getlist?pnum=1&psize=200&lastloc=false&acc_id=sample');
    });

    it("should throw error if code 400",async () => {
        var obj = {
            status: {
                code: 400
            }
        }
        fetch.once(JSON.stringify(obj));
        await expect(getAllVehicles({ accountId: "sample"})).rejects.toEqual(obj);
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/vehicle/getlist?pnum=1&psize=200&lastloc=false&acc_id=sample');
    });
});