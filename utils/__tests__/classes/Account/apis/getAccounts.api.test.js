import getAccounts from '../../../../src/classes/Account/apis/getAccounts.api';
describe("Account List api", () => {
    beforeEach(()=>{
        fetch.resetMocks();
    });
    

    it("should not throw error if empty options",async () => {
        fetch.once(JSON.stringify({
            status: {
                code: 200
            }
        }));
        var data =await getAccounts({}); 
        expect(data).toBeDefined();
        expect(data.status.code).toBe(200);
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/account/listV2?&psize=50&acc_ids=&pnum=1&status=*&query=*');
    });

    it("should throw error if code 400",async () => {
        var obj = {
            status: {
                code: 400
            }
        }
        fetch.once(JSON.stringify(obj));
        await expect(getAccounts({})).rejects.toEqual(obj);
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/account/listV2?&psize=50&acc_ids=&pnum=1&status=*&query=*');
    });
});