import getSpecs from '../../../../src/classes/Account/apis/getSpecs.api';
describe("Specs api", () => {
    beforeEach(()=>{
        fetch.resetMocks();
    });

    it("should not throw error if empty options",async () => {
        fetch.once(JSON.stringify({
            status: {
                code: 200
            }
        }));
        var data =await getSpecs({ accountId: "sample" }); 
        expect(data).toBeDefined();
        expect(data.status.code).toBe(200);
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/account/sample/specsV2?&acc_id=sample');
    });

    it("should throw error if code 400",async () => {
        var obj = {
            status: {
                code: 400
            }
        }
        fetch.once(JSON.stringify(obj));
        await expect(getSpecs({ accountId: "sample" })).rejects.toEqual(obj);
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/account/sample/specsV2?&acc_id=sample');
    });
});