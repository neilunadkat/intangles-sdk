import login from '../../../../src/classes/User/apis/login.api';
describe("Login api", () => {
    beforeEach(()=>{
        fetch.resetMocks();
    });
    
    it("Login with incorrect data",async () => {
        var obj = {
            "status": {
                "code": "12004",
                "msg": "Invalid credentials"
            }
        };
        fetch.once(JSON.stringify(obj));
        var data = login({
            username: "test",
            pwd: "test"
        });
        await expect(data).rejects.toEqual(obj);
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/v2/user/login');
    });
});