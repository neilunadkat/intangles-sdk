import getDtcInfos from '../../../../src/classes/Dtc/apis/getDtcInfos.api';
describe("AlertLog List api", () => {
    beforeEach(()=>{
        fetch.resetMocks();
    });

    it("should not throw error if empty options",async () => {
        fetch.once(JSON.stringify({
            status: {
                code: 200
            }
        }));
        var data =await getDtcInfos({ accountId : "sample"}); 
        expect(data).toBeDefined();
        expect(data.status.code).toBe(200);
        expect(fetch.mock.calls[0][0]).toBe("https://apis.intangles.com/dtc/listV2?codes=&man=&protocol=&acc_id=sample");
    });

    it("should throw error if code 400",async () => {
        var obj = {
            status: {
                code: 400
            }
        }
        fetch.once(JSON.stringify(obj));
        await expect(getDtcInfos({ accountId : "sample"})).rejects.toEqual(obj);
        expect(fetch.mock.calls[0][0]).toBe("https://apis.intangles.com/dtc/listV2?codes=&man=&protocol=&acc_id=sample");
    });

    it("should call api correctly with vehicles",async () => {
        fetch.once(JSON.stringify({
            status: {
                code: 200
            }
        }));
        var data =await getDtcInfos({
            codes: ["vehicle"],
            accountId: "sample"
        }); 
        expect(data).toBeDefined();
        expect(data.status.code).toBe(200);
        expect(fetch.mock.calls[0][0]).toBe("https://apis.intangles.com/dtc/listV2?codes=vehicle&man=&protocol=&acc_id=sample");
    });
});