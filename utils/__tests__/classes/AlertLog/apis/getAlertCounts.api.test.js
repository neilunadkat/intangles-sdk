import getAlertCounts from '../../../../src/classes/AlertLog/apis/getAlertCounts.api';
describe("AlertLog List api", () => {
    beforeEach(()=>{
        fetch.resetMocks();
    });
    
    it("should not throw error if empty options",async () => {
        fetch.once(JSON.stringify({
            status: {
                code: 200
            }
        }));
        var data =await getAlertCounts({
            accountId: "sample"
        }); 
        expect(data).toBeDefined();
        expect(data.status.code).toBe(200);
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/alertlog/logsV2/undefined/undefined/count?&psize=20&pnum=1&types=over_speed,speeding,idling,stoppage,hard_brake,freerun&acc_id=sample');
    });

    it("should throw error if code 400",async () => {
        var obj = {
            status: {
                code: 400
            }
        }
        fetch.once(JSON.stringify(obj));
        await expect(getAlertCounts({
            accountId: "sample"
        })).rejects.toEqual(obj);
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/alertlog/logsV2/undefined/undefined/count?&psize=20&pnum=1&types=over_speed,speeding,idling,stoppage,hard_brake,freerun&acc_id=sample');
    });

    it("should call api correctly with vehicles",async () => {
        fetch.once(JSON.stringify({
            status: {
                code: 200
            }
        }));
        var data =await getAlertCounts({
            vehicles: "vehicle",
            accountId: "sample"
        }); 
        expect(data).toBeDefined();
        expect(data.status.code).toBe(200);
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/alertlog/logsV2/undefined/undefined/count?&psize=20&pnum=1&types=over_speed,speeding,idling,stoppage,hard_brake,freerun&vehicles=vehicle&acc_id=sample');
    });
});