import getAlertLogs from '../../../../src/classes/AlertLog/apis/getAlertLogs.api';
describe("AlertLog List api", () => {
    beforeEach(()=>{
        fetch.resetMocks();
    });


    it("should not throw error if empty options",async () => {
        fetch.once(JSON.stringify({
            status: {
                code: 200
            }
        }));
        var data =await getAlertLogs({
            accountId: "sample"
        }); 
        expect(data).toBeDefined();
        expect(data.status.code).toBe(200);
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/alertlog/logsV2/undefined/undefined?&psize=20&pnum=1&types=over_speed&acc_id=sample');
    });

    it("should throw error if code 400",async () => {
        var obj = {
            status: {
                code: 400
            }
        }
        fetch.once(JSON.stringify(obj));
        await expect(getAlertLogs({
            accountId: "sample"
        })).rejects.toEqual(obj);
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/alertlog/logsV2/undefined/undefined?&psize=20&pnum=1&types=over_speed&acc_id=sample');
    });

    it("should call api correctly with vehicles",async () => {
        fetch.once(JSON.stringify({
            status: {
                code: 200
            }
        }));
        var data =await getAlertLogs({
            vehicles: "vehicle",
            accountId: "sample"
        }); 
        expect(data).toBeDefined();
        expect(data.status.code).toBe(200);
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/alertlog/logsV2/undefined/undefined?&psize=20&pnum=1&types=over_speed&vehicles=vehicle&acc_id=sample');
    });
});