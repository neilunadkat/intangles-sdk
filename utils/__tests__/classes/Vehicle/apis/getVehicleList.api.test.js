import getVehicleList from '../../../../src/classes/Vehicle/apis/getVehicleList.api';
describe("Vehicle List api", () => {
    beforeEach(()=>{
        fetch.resetMocks();
    });
    
    it("should not throw error if empty options",async () => {
        fetch.once(JSON.stringify({
            status: {
                code: 200
            }
        }));
        var data =await getVehicleList({accountId: "sample"}); 
        expect(data).toBeDefined();
        expect(data.status.code).toBe(200);
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/vehicle/getlist?pnum=1&psize=50&lastloc=false&acc_id=sample');
    });

    it("should throw error if code 400",async () => {
        var obj = {
            status: {
                code: 400
            }
        }
        fetch.once(JSON.stringify(obj));
        await expect(getVehicleList({accountId: 'sample'})).rejects.toEqual(obj);
        expect(fetch.mock.calls[0][0]).toBe('https://apis.intangles.com/vehicle/getlist?pnum=1&psize=50&lastloc=false&acc_id=sample');
    });
});