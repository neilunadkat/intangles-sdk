import Intangles from '../src/index';
import generateFiles from '../generateFiles';
describe("API SDK", ()=>{
    var IntanglesObject = null;
    beforeAll(()=>{
        generateFiles();
    });
    beforeEach(()=>{
        IntanglesObject = new Intangles({baseUrl:'base url',token:'token'});        
    })
    it("should be a function",()=>{
        expect(Intangles).toBeDefined();
        expect(typeof Intangles).toBe('function');
    });
    it("should check if functions are created correctly",()=>{
        IntanglesObject = new Intangles({baseUrl:'base url',token:'token'}); 
        expect(IntanglesObject.Vehicle.getVehicleList).toBeDefined();
        expect(typeof IntanglesObject.Vehicle.getVehicleList).toBe("function");
        expect(IntanglesObject.Account.getAccounts).toBeDefined();
        expect(typeof IntanglesObject.Account.getAccounts).toBe("function");
    })
})