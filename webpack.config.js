var path = require('path');
var generateFiles = require("./utils/generateFiles");
var prod = process.env.NODE_ENV == 'production';
var ncp = require('ncp').ncp;
var fs = require('fs');
if(!fs.existsSync('./utils/src'))
  fs.mkdirSync('./native');
ncp.limit = 16;
  
ncp('./utils/src', './native', function (err) {
  if (err) {
    return console.error(err);
  }
  console.log('Generated native folder!');
});
generateFiles();
module.exports = {
  entry: ['./utils/src/index.js'],
  output: {
    library: 'Intangles',
    path: path.resolve(__dirname,'dist'),
    filename: 'IntanglesSDK.'+(prod? 'min.': '')+'js'
  },
  module: {
    rules: [
      { test: /\.js$/, use: 'babel-loader' }
    ],
  },
  resolve: {
    modules: [path.resolve(__dirname), "node_modules"],    
  },
  mode: process.env.NODE_ENV || 'development'
};